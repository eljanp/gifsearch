//homework
//only display the preview-window once search results have been populated
//when you click on an image, save the image to a different array and display it permanently on the page (until refresh), and then hide the preview window.
//use the text input field to define the search parameters
//make it look fancy with additional css
 const app = new Vue({
      el: '#app',
      data: {
        // test giphy api key
        // if the usage limit has been hit, visit https://developers.giphy.com to get a new one
        // 80bfcbf357864cd18518c324f47a7098
        apiUrl: 'http://api.giphy.com/v1/gifs',
        apiKey: '80bfcbf357864cd18518c324f47a7098',
        searchedGifs: null,
        trendingGifs: null,
        query: ''
      },
      methods: {
       
        searchGifs: function() {
          const url = `${this.apiUrl}/search?api_key=${this.apiKey}&q=${this.query}&limit=20`;
          fetch(url)
            .then(response => response.json())
            .then(data => this.searchedGifs = data.data);
        }
      },
      created: function() {
     
      }
    });